                                    MANIFIESTO DEL SELFHOSTING

Por debajo de las oscuras e inciertas nubes de las bigtech, escondida entre los
desclasados obreros del byte y los falsos profetas tecnológicos que con cantos
de sirenas ofrecen sus servicios digitales para “facilitar” la vida digital, se
alza una comunidad anárquica y contracultural que busca recuperar la Internet y
luchar contra aquellos que exprimen nuestra identidad en forma de datos para
generar riquezas y publicidad para la manipulación y cohersión social masiva.
Navegando por la red de redes, con una pequeña flota de servidores
autogestionados, geográficamente distribuidos aunque cohesivamente unidos por el
cyberespacio, la comunidad de selfhosting surge como un estilo de vida, una
lógica de habitar lo digital, una forma de militar por una red abierta, humana y
libre de la oligarquía del dato.

A la naturalización de la ya cristalizada frase "la nube es la computadora de
otro” le agregamos que ese "otro" no es más que un conglomerado de corporaciones
que como un kraken hambriento devora y controla los océanos del cyberespacio.
Contra eso nos armamos en acción comunitaria, directa y autogestionada por y
para quienes habitamos y peleamos por una Internet más soberana y justa.
Nuestros objetivos son claros, y nuestros principios, precisos. Buscamos romper
el espejismo y el encanto que estas bestias impusieron a punta de ISPs y
blacklist y propiciamos el ideal de una comunidad organizada en base a sus
necesidades informáticas sin la intermediación de forajidos y vendehumos del
byte.

Las grandes tecnológicas desembarcaron en la red con una miríada de servicios
gratuitos que venían a reemplazar estándares establecidos durante años de
trabajo entre usuarios, desarrolladores, comunidades, tecnócratas y demás
entusiastas de la marea sideral del cyberespacio. Mercantilizando los servicios
básicos de Internet y transformándolos en objetos de consumo, nos fueron
llevando a sus islas de productos estilizados, construidos íntegramente con el
objetivo de comercializar cada aspecto de nuestras vidas para intentar digitar y
direccionar nuestros consumos. Enviar un correo electrónico, chatear con
familiares y amig@s, guardar archivos en la red o simplemente compartir un link,
todo pasó a estar debidamente indexado, taggeado y procesado por la computadora
de otro. Un otro que no es un amigo, ni un familiar, ni nadie conocido, sino una
megacorporación que a base decisiones fríamente calculadas intenta manipular y
modificar nuestros hábitos y consumos. Aquí no hay quien se salve, cualquiera
que haya habitado esos espacios digitales ha visto cómo estos servicios han
cambiado nuestras conductas sociales y percepciones de la realidad, ¿o acaso
seguiremos haciendo la vista gorda en torno a los tremendos trastornos que las
redes sociales generan en toda la juventud o el despilfarro absurdo de recursos
que implica sostener las aplicaciones de las megaempresas tecnológicas? Quizás
aquellos que tanto alaban a los tecnogurúes del Valle del Silicio no vean el
descalabro que significa tener que cambiar de celular o computadora porque
simplemente ya no podes navegar la web ni mandar un correo electrónico.

Si este es el tecnosolucionismo que los cryptoentusiastas, evangelistas de la
web del futuro o falsos chamanes de la programación nos ofrecen, lo rechazamos
de plano. Somos hacktivistas y militantes de base del software libre:
nos apropiamos de la tecnología en pos de buscar una construcción colectiva
acorde a nuestras comunidades y no a los espurios designios de un
hipermercantilizado mercado de la informática. Si hoy en día el obrero del byte
cumple el mismo rol que el carbonero o tallerista de fines del siglo XIX,
resulta imperioso que se politice y apropie de los medios de producción para
construir una alternativa a esta violencia del dato. Sólo cuando esta masa
enorme de obreros informáticos despierten del letargo podremos dar el paso
siguiente hacia la refundación de un cyberespacio.

Pero no hay que construir sobre el océano vacío, como si estuviésemos perdidos
en ultramar lejos de toda costa; existe ya una pequeña pero sólida flota de
islas nómades, que esquivan y cercenan los tentáculos del kraken de las big
tech. Aquellas islas son las computadoras de otros, pero otros reales,
autogestionados y organizados en pos de necesidades personales, comunitarias y
sociales. El selfhosting consiste en materializar aquello conocido como "la
nube", pero despojada ya de la tiranía del dato y del derroche energético al que
nos tienen acostumbrados las grandes empresas tecnológicas. Ellas no están
organizadas para mercantilizar nuestras identidades, sino para brindar servicios
de correo electrónico, chat, alojamiento de archivos, chat de voz o cualquier
otra necesidad digital existente. Nuestras pequeñas islas-servidores demuestran
que es posible mantenerse activo en la red sin el trackeo y el robo violento, ni
la necesidad impuesta de recambiar constantemente nuestros equipos informáticos:
los servicios autohosteados, al ser pensados por y para la comunidad, se piensan
desde la mayor eficiencia posible y no el inmoral derroche que colabora
directamente con la crisis climática.

Por eso, obrera y obrero del byte desclasado te decimos, ¡formate, cuestionate,
y apropiate de las herramientas que utilizás en pos de conformar una
mancomunidad de hacktivistas! Sólo entre la unión de los obreros y obreras
informáticas y las comunidades de selfhosting y hacktivismo podremos construir
alternativas para la refundación de un cyberespacio al servicio de las personas
y no de la oligarquía del byte.

Pero no solamente necesitamos de la masa obrera sino también de los ciudadanos y
ciudadanas digitales de a pie, ¡despertemos de la apatía generalizada a la que
nos han acostumbrado! Nadie puede ya decir que la tecnología no es lo suyo o que
la informática no le importa cuando todas nuestras vidas están mediadas a través
de sistemas digitales. Ese télefono android que aún vive pero que ya no te
permite ver tus correos o chatear con tu familia es simplemente la realidad
tecnológica pegándote en la cara; tanto así como la ansiedad o dispersión que
existen en vos desde los últimos 15 años. ¡Imaginá el cerebro de un adolescente
de 14 años, totalmente apolillado por los violentos algoritmos de las big tech!

Las necesidades digitales comunitarias se dirimen en las costas de nuestras
islas-servidores, no en los buques insignias de las refinerías de datos.
Unámonos construyendo pequeños servidores en nuestras casas, lugares de trabajo
o espacios culturales; unámonos construyendo redes de datos que brinden
servicios públicos de mensajería instantánea federada que realmente respeten
nuestras libertades y privacidad. Publiquemos servicios de voz robustos y con
baja latencia; propiciemos el uso de servicios de bajo consumo de cómputo para
democratizar las voces sin importar si usás un bote o un barco de competición de
última generación. Creemos foros especializados e interconectemos comunidades
para unirnos entre todos, icemos nuestras velas con los protocolos y estándares
que existen, los cuales nos permiten bucear la red utilizando el dispositivo que
queramos y no el que nos impongan. Perdamos el miedo que no nos deja dar el
primer paso e iniciar este gran camino de aprendizaje, que como beneficio extra
nos hará volver a recuperar no solo nuestra soberanía tecnológica sino el
control de nuestra esencia digital. No se trata de cortar de lleno con las redes
de datos privadas de las big tech sino más bien de ir construyendo desde las
bases hacktivistas, en mancomunión con los obreros y obreras del byte y la
ciudadanía digital espacios autogestionados, autoalojados y autoadministradas:
una Internet de la comunidad para la comunidad.

## ¿Te querés adherir al manifiesto?

Es tan simple como clonar, editar el manifiesto o agregarte a vos y/o a la
comunidad que representás al final de la siguiente lista y enviarnos el Pull
Request!

## ¿Quiénes adhieren?

- Pirates @board [Cyberdelia](https://cyberdelia.com.ar).
- Hacktivistas y piratas de
  [Cybercirujas](https://cybercirujas.rebelion.digital/foro)
- [Cuates.net](https://cuates.net)
- [Categulario](https://mstdn.mx/@categulario)
- [HackspaceUY](https://hackspace.uy)
- [Resistencia Programada](https://resistenciaprogramada.org)
- [Hackfun Rosario](https://hackfunrosario.com)
- [Matiargs](https://matiargs.com)
